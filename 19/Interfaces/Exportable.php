<?php
interface Exportable{
	public function import($data);
	public function export();
}

interface Removable{
	public function remove();
}

class TEXT_Exporter implements Exportable {
	public static $extention = ".txt";
	public $data;

	public  function import( $data ) {
		$this->data = $data;
	}

	public function export() {
		$str = '';
		foreach ($this->data as $row){
			foreach ($row as $cell){
				$str .= $cell . ",";
			}
			$str .=  PHP_EOL;
		}
		var_dump($this->data);
		var_dump($str);

		file_put_contents("file-".rand(100,999).static::$extention,$str);
	}

}


$text_exporter = new TEXT_Exporter();
$text_exporter->import([[1,2,3],[1,2,3]]);
$text_exporter->export();




class User implements Exportable,Removable,Loggable {
	public function import( $data ) {

	}

	public function export() {

	}
	public function remove() {

	}

}