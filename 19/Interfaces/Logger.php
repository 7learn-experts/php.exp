<?php
class Logger{
	public static function log($level,$msg) {
		echo date() . " - $level : $msg";
	}
	public static function error($msg) {
		echo date() . " - Error : $msg";
	}
	public static function warning($msg) {
		echo date() . " - Warning : $msg";
	}
}