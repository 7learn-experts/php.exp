<?php
session_start();

$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = '';
$db     = 'myfirstwebsite';
$conn   = mysqli_connect( $dbhost, $dbuser, $dbpass, $db );

if ( ! $conn ) {
	echo 'Connected failure<br>';
	die();
}
mysqli_set_charset($conn,"utf8");

include_once "lib/auth.php";