<?php
include "../common/common.php";

$target_dir = "files/";

//$fileName = isset($_FILES["fileToUpload"]["name"]) ? $_FILES["fileToUpload"]["name"] : rand(1000,9999);
//$fileName = $_FILES["fileToUpload"]["name"] ?? rand(100,999);

$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$err_messages = [];
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
	$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
	if($check !== false) {
		echo "File is an image - " . $check["mime"] . ".";
		$uploadOk = 1;
	} else {
		$err_messages[] = "File is not an image.";
		$uploadOk = 0;
	}
}
// Check if file already exists
if (file_exists($target_file)) {
/*	$err_messages[] = "Sorry, file already exists.";
	$uploadOk = 0;*/
	$target_file = $target_dir . md5(rand(10000,9999999)) . "." . $imageFileType ;

}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 50000000) {
	$err_messages[] = "Sorry, your file is too large.";
	$uploadOk = 0;
}
// Allow certain file formats
if(!in_array($imageFileType,["jpg","jpeg","png","gif"])) {
	$err_messages[] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
	$uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
	foreach ( $err_messages as $err_message ) {
		echo "<div style='color:red'>".$err_message."</div>";
	}
// if everything is ok, try to upload file
} else {
	if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
		echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
	} else {
		echo "Sorry, there was an error uploading your file.";
	}
}
?>