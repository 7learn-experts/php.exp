<?php
error_reporting( E_ALL );
ini_set( 'display_errors', 1 );

class Car {
	public $color = "red";
	public $brand;
	private $price;
	public $engine;
	private $chassisID = '35447567658';

	/**
	 * Car constructor.
	 *
	 * @param string $color
	 * @param $brand
	 * @param $price
	 * @param $engine
	 * @param string $chassisID
	 */
	public function __construct( string $color, $brand, $price, $engine, string $chassisID ) {
		$this->color     = $color;
		$this->brand     = $brand;
		$this->price     = $price;
		$this->engine    = $engine;
		$this->chassisID = $chassisID;
	}


	public function getPrice() {
		return $this->price;
	}

	/**
	 * @return string
	 */
	public function getColor(): string {
		return $this->color;
	}

	/**
	 * @param string $color
	 */
	public function setColor( string $color ): void {
		$this->color = $color;
	}

	/**
	 * @return mixed
	 */
	public function getBrand() {
		return $this->brand;
	}

	/**
	 * @param mixed $brand
	 */
	public function setBrand( $brand ): void {
		$this->brand = $brand;
	}

	/**
	 * @return mixed
	 */
	public function getEngine() {
		return $this->engine;
	}

	/**
	 * @param mixed $engine
	 */
	public function setEngine( $engine ): void {
		$this->engine = $engine;
	}


	public function setPrice($price) {
		if(true){
			$this->price = $price;
		}
	}


	public function start() {
		echo "im strated !<br>";
	}


	public function run() {
		echo "im running ...<br>";
	}

	public static function staticMethod() {
		echo "Hi from staticMethod()<br>";
	}

	private function getChassis() {
		return $this->start();
	}

}


$a = 1;
$s = "hi";

$f = function () {
	echo "im f()";
};
$f();

/*$pride = new Car();
$bmwX3 = new Car();*/

$bmwX4 = new Car("red","BMW",99999,"ef7",rand(9999,999999));
$bmwX4->setPrice(10000);
echo $bmwX4->getPrice()."<br>";
//echo $bmwX4->color;
$bmwX4->start();
$bmwX4->run();
