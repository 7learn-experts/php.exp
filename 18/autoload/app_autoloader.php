<?php

function my_autoload($class){

	$class_path =  "$class.php";
	echo "$class ($class_path)<br>";
	if(file_exists($class_path) and is_readable($class_path)){
		require_once $class_path;
	}else{
		// error
	}
}

spl_autoload_register('my_autoload');
