<?php

abstract class Exporter{
	public  static $data ;
	public static $extention ;
	public function __construct($data) {
		self::$data = $data;
	}

	public static function import($data) {
		self::$data = $data;
	}

	public static abstract function export();
}


class CSV_Exporter extends TEXT_Exporter {
	public static $extention = ".csv";
	public function __construct( $data ) {
		parent::__construct( $data );
	}

}
class TEXT_Exporter extends Exporter{
	public static $extention = ".txt";

	public static function export() {
		$str = '';
		foreach (self::$data as $row){
			foreach ($row as $cell){
				$str .= $cell . ",";
			}
			$str .=  PHP_EOL;
		}
		file_put_contents("file-".rand(100,999).static::$extention,$str);
	}

}

class JSON_Exporter extends TEXT_Exporter {
	public static $extention = ".json";
	public static function export() {
		file_put_contents("file-".rand(100,999).static::$extention,json_encode(self::$data));
	}
}

class PDF_Exporter extends Exporter{
	public static function export() {
		echo "PDF Data";
	}
}

$data = [["C1","C2","C3"],[1,2,3],[2,3,4],[7,8,9]];
$csvExporter = new CSV_Exporter($data);
$csvExporter::export();

$txtExporter = new TEXT_Exporter($data);
$txtExporter::export();

$jsonExporter = new JSON_Exporter($data);
$jsonExporter::export();








