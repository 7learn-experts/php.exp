<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Exporter</title>
</head>
<body>
<?php
    if($_SERVER['REQUEST_METHOD'] == "POST"){
        include  "abstractSample.php";
        $method = $_POST['ExportType'] . "_Exporter::export";
        $method();
    }
?>
<div class="wrapper" style="width: 50%;margin: 50px auto">
    <form action="" method="post" id="calcForm">
        <select name="ExportType" >
            <option value="CSV">Excel</option>
            <option value="TEXT">Text</option>
            <option value="JSON">Json</option>
            <option value="PDF">PDF</option>
        </select>
        <input type="submit" name="submit" value="Export">
    </form>
    </div>
</div>


</body>
</html>