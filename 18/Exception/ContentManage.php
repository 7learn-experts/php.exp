<?php
/*class ContentManager{
	public $content;
	const MIN_ACCESS_AGE = 18;

	public function getContent($userAge){
		if($userAge >= MIN_ACCESS_AGE){
			echo $this->content;
		}else{
			throw new Exception("under 18 Not Allowed!",1001);
		}
	}
}*/
error_reporting( E_ALL );
ini_set( 'display_errors', 1 );

class Under18NotAllowedException extends Exception {

	protected $message = "under 18 Not Allowed!";
	protected $code = 1001;


	public function nicePrintMessage(): void {
		echo "<div style='color:green'>$this->message</div>";
	}
}


function getContentFunc($userAge){
	if($userAge >= 18){
		echo "Content";
	}else{
		throw new Under18NotAllowedException();
	}
}

//$cm = new ContentManager();

try{
//	$cm->getContent(14);
	getContentFunc(12);
}catch (Under18NotAllowedException $e){
	echo $e->nicePrintMessage();
}


